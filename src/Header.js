import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {mainListItems , secondaryListItems } from './listitem';
import PropsTypes from 'prop-types';
import classNames from 'classnames';
import PeopleIcon from '@material-ui/icons/People';
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';
import { withStyles } from '@material-ui/core/styles';
import Newfeed from './Newfeed';
import Post from './Post';
const drawerWidth = 250;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 100, 
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 10px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
});



class Header extends Component {
	state={
		open: true,
	};
	handleDrawerOpen=()=>{
		this.setState({ open:true });
	};
	handleDrawerClose=()=>{
		this.setState({ open:false });
	};


  render() {
  	const {classes}=this.props;
    return (
    		<div className={classes.root}>
    		<CssBaseline />
    		<AppBar
    		position="absolute"
    		className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
    		>

    		<Toolbar disableGutters={!this.state.open} className={classes.toolbar}>   
    		<IconButton
    		color="inherit"
    		onClick={this.handleDrawerOpen}
    		className={classNames(
    			classes.menuButton,
    			this.state.open && classes.menuButtonHidden,
)}
    		>
    		<MenuIcon />
    		</IconButton>
    		<Typography 
    		component="h1"
    		variant="h5"
    		color="inherit"
    		noWrap
    		className={classes.title}>
    		Myanmar Gateway Engineering & Consultancy Group
    		</Typography>
    		<IconButton color="inherit">
    		<HomeIcon />
    		</IconButton>
    		<IconButton color="inherit">
    		<Badge badgeContent={4} color="secondary">
    		<NotificationsIcon />
    		</Badge>
    		</IconButton>
    		<IconButton color="inherit">
    		<Badge badgeContent={4} color="secondary">
    		<PeopleIcon />
    		</Badge>
    		</IconButton>
    		<IconButton color="inherit">
    		<SettingsIcon />
    		</IconButton>
    		</Toolbar>
    		</AppBar>
			<Drawer 
				variant="permanent"
			classes={{
			paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
			}}
			open={this.state.open}
			>
			<div className={classes.toolbarIcon}>MGEC Global
			<IconButton onClick={this.handleDrawerClose}>
			<ChevronLeftIcon />
			</IconButton>
			</div>

			<Divider />
			<List>{mainListItems}</List>
			<Divider />
			<List>{secondaryListItems}</List>
			</Drawer>


			<main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Typography variant="h4" gutterBottom component="h2">
            MGEC NewFeed
			</Typography>
			<Post />
				<Newfeed />

            <Divider/>
			<Typography variant="h4" gutterBottom component="h2">
            Products
			</Typography>
			</main>
    		</div>


    );
  }
}
Header.PropsTypes ={
	classes: PropsTypes.object.isRequired,
};
export default withStyles(styles)(Header);
